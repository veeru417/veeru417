public without sharing class MyTriggerHandler
{

        public static Boolean bypassTrigger() 
            {
                return bypassTriggerCode();
            }


        private static Boolean bypassTriggerCode() 
            {
               // bypass logic if custom setting is enabled for user or profile
                CustomePrivilege__c cpriv_profile = CustomePrivilege__c.getInstance(UserInfo.getProfileId());
                
                CustomePrivilege__c cpriv_user = CustomePrivilege__c.getInstance(UserInfo.getUserId());
                
                     if (cpriv_user.OverrideAll__c || cpriv_profile.OverrideAll__c ) 
                
                         return true;
                             else
                         return false;
            }

 }