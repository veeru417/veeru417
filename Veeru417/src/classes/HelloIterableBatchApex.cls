global class HelloIterableBatchApex implements Database.Batchable<Proj__c> {
  global Iterable<Proj__c> start(Database.BatchableContext context) {
    System.debug('start');
    return new ProjectIterable();
  }
  global void execute(Database.BatchableContext context, List<Proj__c> scope) {
    System.debug('execute');
    for(Proj__c rec : scope) {
      System.debug('Project: ' + rec.Name);
    }
  }
  global void finish(Database.BatchableContext context) {
    System.debug('finish');
  }
}