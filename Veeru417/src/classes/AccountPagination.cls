public with sharing class AccountPagination {
    private final Account acct;  

    public AccountPagination(
           ApexPages.StandardSetController controller) 
    {
           this.acct = (Account)controller.getRecord(); 
    }    
    
    public ApexPages.StandardSetController accountRecords{
        get {
            if(accountRecords == null) {
                return new ApexPages.StandardSetController(
                         Database.getQueryLocator(
                [SELECT name FROM Account WHERE Id NOT IN 
                (SELECT AccountId FROM Opportunity 
                 WHERE IsClosed = false)]));
            }
            return accountRecords;
        }
        private set;
    }
    public List<Account> getAccountPagination() {
         return (List<Account>) accountRecords.getRecords();
    }  
}