public class Chapter13 {
	
/** Listing 13-1 */
  public static void listing13_1() {
    List<ContactFeed> result = [ SELECT ParentId, FeedPost.Body, Type, CreatedBy.Name, CreatedDate
      FROM ContactFeed
      ORDER BY CreatedDate DESC LIMIT 10 ];
  }

/** Listing 13-2
public Id post(Id recordId, String text) {
  FeedItem post = new FeedItem(ParentId = recordId, Body = text);
  insert post;
  return post.Id;
}
*/

/** Listing 13-3
public void deleteContactPost(Id postId) {
  ContactFeed post = [ SELECT Id FROM ContactFeed
    WHERE FeedPostId = :postId ];
  delete post;
}
*/

/** Listing 13-4 */
  public static void listing13_4() {
    List<Proj__Feed> result = [ SELECT ParentId, FeedPost.Body, Type, CreatedBy.Name, CreatedDate
      FROM Proj__Feed ];
  }

/** Listing 13-5 */
  public static void listing13_5() {
    List<UserFeed> result = [ SELECT ParentId, FeedPostId, Type, CreatedById, CreatedDate
      FROM UserFeed ];
  }

/** Listing 13-6 */
  public static void listing13_6() {
    List<UserProfileFeed> result = [ SELECT ParentId, FeedPostId, Type, CreatedById, CreatedDate
      FROM UserProfileFeed
      WITH UserId = '00580000001opOPAAY' ];
  }

/** Listing 13-7 */
  public static void listing13_7() {
    List<NewsFeed> result = [ SELECT ParentId, FeedPost.Body, Type, CreatedBy.Name, CreatedDate
      FROM NewsFeed ];
  }

/** Listing 13-8 */
  public static void listing13_8() {
    List<Proj__Feed> result = [ SELECT ParentId, Type, CreatedById, CreatedDate, FeedPost.Body, 
      (SELECT CommentBody, CreatedById, CreatedDate FROM FeedComments)
      FROM Proj__Feed ];
  }

/** Listing 13-9
public Id comment(Id postId, String text) {
  FeedComment comment = new FeedComment(
    FeedItemId = postId, CommentBody = text);
  insert comment;
  return comment.Id;
}
*/

/** Listing 13-10
public void deleteComment(Id postId, Id commentId) {
  Proj__Feed post = [ SELECT Id,
    (SELECT Id from FeedComments WHERE Id = :commentId)
    FROM Proj__Feed WHERE FeedPostId = :postId ];
  delete post.FeedComments[0];
}
*/

/** Listing 13-11
SELECT ParentId, Type, CreatedById, CreatedDate,
  (SELECT FeedItemId, FieldName, OldValue, NewValue
    FROM FeedTrackedChanges)
  FROM ContactFeed
*/

/** Listing 13-12
SELECT ParentId, SubscriberId, CreatedById, CreatedDate
  FROM EntitySubscription
*/


/** Listing 13-13
public Id follow(Id recordId, Id userId) {
  EntitySubscription e = new EntitySubscription(
    ParentId = recordId, SubscriberId = userId);
  insert e;
  return e.Id;
}
*/

/** Listing 13-14
public void unfollow(Id subscriptionId) {
  delete [ SELECT Id FROM EntitySubscription
    WHERE Id = :subscriptionId ];
}
*/

/** Listing 13-15
<apex:page standardController="Proj__c">
  <apex:sectionHeader title="Project"
    subtitle="{!record.Id}" />
  <apex:pageBlock title="Chatter Components">
    <chatter:feedWithFollowers entityId="{!record.Id}" />
  </apex:pageBlock>
</apex:page>
*/

/** Listing 13-16
public with sharing class FollowProjectControllerExtension {
  private ApexPages.StandardController controller;
  public FollowProjectControllerExtension(
    ApexPages.StandardController stdController) {
    this.controller = stdController;
  }
  public PageReference followProject() {
    Id currentUserId = UserInfo.getUserId();
    Set<Id> userIds = new Set<Id>();
    for (List<Assignment__c> assignments :
      [ select Resource__r.User__c from Assignment__c where
         Project__c = :controller.getRecord().Id ]) {
      for (Assignment__c assignment : assignments) {
        Id uid = assignment.Resource__r.User__c;
        if (currentUserId != uid && uid != null) {
          userIds.add(uid);
        }
      }
    }
    if (userIds.size() == 0) {
      error('Project has no assignments.');
      return null;
    }
    Set<String> subs = new Set<String>();
    for (List<EntitySubscription> recs :
      [ select ParentId from EntitySubscription
        where SubscriberId = :currentUserId 
        and ParentId in :userIds ]) {
      for (EntitySubscription rec : recs) {
        subs.add(rec.ParentId);
      }
    }
    Integer followCount = 0;
    List<EntitySubscription> adds = new List<EntitySubscription>();
    for (Id userId : userIds) {
      if (!subs.contains(userId)) {
        adds.add(new EntitySubscription(
          ParentId = userId, SubscriberId = currentUserId));
        followCount++;
      }
    }
    insert adds;
    info(followCount + ' users followed');
    return null;
  }
  private static void info(String text) {
    ApexPages.Message msg = new ApexPages.Message(
      ApexPages.Severity.INFO, text);
    ApexPages.addMessage(msg);
  } 
  private static void error(String text) {
    ApexPages.Message msg = new ApexPages.Message(
      ApexPages.Severity.ERROR, text);
    ApexPages.addMessage(msg);
  } 
}
*/

/** Listing 13-17
<apex:page standardController="Proj__c"
  extensions="FollowProjectControllerExtension"
  action="{!followProject}">
  <apex:pageMessages />
</apex:page>
*/

  testmethod public static void test() {
  	listing13_1();
  	listing13_4();
  	listing13_5();
  	listing13_6();
  	listing13_7();
  	listing13_8();
  }
}