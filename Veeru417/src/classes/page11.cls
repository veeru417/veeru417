public class page11 {
public list<Contact> con;
public String str{get;set;}
 public String id2 { get; set; }
 
 List<contactwrapper> contactList{get;set;}
    List<contact> selectedContacts = new List<contact>();
   public List< Contact> f{get;set;}
   
   public string rowtoshow{get;set;}
  
  
    public String name { get; set; }
 
    public String id { get; set; }
 
    public String param1{get;set;}
    
    
    public List<contactwrapper> getContacts()
 
    {
        contactList=new List<contactwrapper >();
       
        param1=ApexPages.currentPage().getParameters().get('');
      
        for(contact a : [select Id, Name,Email,FirstName,LastName from  Contact where account.id =:param1 limit 5])
 
        contactList.add(new contactwrapper(a));
 
        return contactList;
 
    }
 
    
    public PageReference getSelected()
 
    {
      
        selectedContacts.clear();
 
        for(contactwrapper conwrapper : contactList)
 
        if(conwrapper.selected == true)
        {
      
 
        selectedContacts.add(conwrapper.cont);
     }
        return null;
 
    }
    public PageReference showSelected()
 
    {
      
        selectedContacts.clear();
      contact con=[select Id, Name,Email,FirstName,LastName from  Contact where id =:rowtoshow];
      
      selectedContacts.add(con);
        
        return null;
 
    }
 
    
 
    public List<Contact> GetSelectedContacts()
 
    {
 
        if(selectedContacts.size()>0)
 
        return selectedContacts;
 
        else
 
        return null;
 
    }  
 
 
    public class contactwrapper
 
    {
 
        public Contact cont{get; set;}
 
        public Boolean selected {get; set;}
 
      public contactwrapper(Contact a)
 
        {
 
            cont = a;
 
            selected = false;
 
        }
 
    }
}