public class wrapperClassController {

    //Our collection of the class/wrapper objects cContact 
    public List<aAccount> AccountList {get; set;}

    //This method uses a simple SOQL query to return a List of Contacts
    public List<aAccount> getAccount() {
        if(AccountList == null) {
            AccountList = new List<aAccount>();
            for(Account a : [select Id, Name,Phone from Account limit 10]) {
                // As each contact is processed we create a new cContact object and add it to the contactList
                AccountList.add(new aAccount(a));
            }
        }
        return AccountList;
    }


    public PageReference processSelected() {

                //We create a new list of Contacts that we be populated only with Contacts if they are selected
        List<Account> selectedAccounts = new List<Account>();

        //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
        for(aAccount aAcc: getAccount()) {
            if(aAcc.selected == true) {
                selectedAccounts.add(aAcc.acc);
            }
        }

        // Now we have our list of selected contacts and can perform any type of logic we want, sending emails, updating a field on the Contact, etc
        System.debug('These are the selected Contacts...');
        for(Account acc: selectedAccounts) {
            system.debug(acc);
        }
        return null;
    }


    // This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Contact and a Boolean value
    public class aAccount {
        public Account acc {get; set;}
        public Boolean selected {get; set;}

        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public aAccount(Account a ) {
            acc = a;
            selected = false;
        }
    }
}