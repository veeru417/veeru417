public class exceptionlog {
    /*
Author - Prajnith K
Descrption - Insert the exceptions while insert/update in to custom object Exception_Log__c
*/
/*
public class ExceptionLog
{

    /*
     On insert Call
     insert the exceptions recevied 
     parameters are saveresult, DML action insert/update and Class/MethodName
    */
   /* 
    public static void insertLog(Database.SaveResult[] results,String DMLtype,String errorFrom)
    { 
             
     list<Exception_Log__c> excepLogs = new list<Exception_Log__c>();
     set<String> errorMessage = new set<String>();
          
               Exception_Log__c excepLog = new Exception_Log__c();
               excepLog.Created_By__c    = userinfo.getUserId();
               excepLog.Date__c          = system.now();
               excepLog.DML_Action__c    = DMLtype;
               excepLog.Error_Message__c = '';
                             
            for (Integer i = 0; i < results.size(); i++) 
            {
                if (!results[i].isSuccess()) // if failed
                { 
                   if(!errorMessage.contains(''+results[i].getErrors()) && excepLog.Error_Message__c.length() <= 31500) // if new error message 
                   {                                                     
                    excepLog.Error_Message__c += errorFrom+'\n'+results[i].getErrors()+'\n \n';   
                    errorMessage.add(''+results[i].getErrors());
                   }                                                                  
                }
            }
            
            if(excepLog.Error_Message__c != null && excepLog.Error_Message__c != '')
                excepLogs.add(excepLog);
                
            if(excepLogs != null && excepLogs.size() > 0 )
            {
                insert excepLogs;
            }
            
    }
    
    /*
     On upsert Call
     insert the exceptions recevied 
     parameters are saveresult, DML action insert/update and Class/MethodName
    */
   /* 
    public static void insertLog(Database.upsertResult [] results,String DMLtype,String errorFrom)
    { 
             
     list<Exception_Log__c> excepLogs = new list<Exception_Log__c>();
     set<String> errorMessage = new set<String>();
          
               Exception_Log__c excepLog = new Exception_Log__c();
               excepLog.Created_By__c    = userinfo.getUserId();
               excepLog.Date__c          = system.now();
               excepLog.DML_Action__c    = DMLtype;
               excepLog.Error_Message__c = '';
                             
            for (Integer i = 0; i < results.size(); i++) 
            {
                if (!results[i].isSuccess()) // if failed
                { 
                   if(!errorMessage.contains(''+results[i].getErrors()) && excepLog.Error_Message__c.length() <= 31500) // if new error message 
                   {                                                     
                    excepLog.Error_Message__c += errorFrom+'\n'+results[i].getErrors()+'\n \n';   
                    errorMessage.add(''+results[i].getErrors());
                   }                                                                  
                }
            }
            
            if(excepLog.Error_Message__c != null && excepLog.Error_Message__c != '')
                excepLogs.add(excepLog);
                
            if(excepLogs != null && excepLogs.size() > 0 )
            {
                insert excepLogs;
            }
            
    }
    
    /*
     On update Call
     insert the exceptions recevied 
     parameters are saveresult, DML action insert/update and Class/MethodName,updating record IDs
    */
   /* 
    public static void insertLog(Database.SaveResult[] results,String DMLtype,String errorFrom,set<ID> DBrecIDs)
    {      
      list<Exception_Log__c> excepLogs = new list<Exception_Log__c>();
      set<String> errorMessage = new set<String>();
          
         Exception_Log__c excepLog = new Exception_Log__c();
               excepLog.Created_By__c    = userinfo.getUserId();
               excepLog.Date__c          = system.now();
               excepLog.DML_Action__c    = DMLtype;
               excepLog.Error_Message__c = '';
     
            for (Integer i = 0; i < results.size(); i++) 
            {
                if (results[i].isSuccess()) // if success get IDs
                {
                   if(DBrecIDs.contains(results[i].getId()))
                   {
                       DBrecIDs.remove(results[i].getId()); // remove success IDs to get Error record IDs
                   }
                }
            }
            
           if(DBrecIDs.size() > 0)
           { 
                for (Integer i = 0; i < results.size(); i++) 
                {
                    if (!results[i].isSuccess()) // if failed
                    {     
                        if(!errorMessage.contains(''+results[i].getErrors()) && excepLog.Error_Message__c.length() <= 31500) // if new error message 
                        {                 
                           excepLog.Error_Message__c += errorFrom+'\n'+results[i].getErrors()+'\n \n';  
                           
                        }                                                                             
                    }
                }
                excepLog.Record_IDs__c    = String.valueof(DBrecIDs);
            }
            
            if(excepLog.Error_Message__c != null && excepLog.Error_Message__c != '')
                excepLogs.add(excepLog);
                
            if(excepLogs != null && excepLogs.size() > 0)
            {
                insert excepLogs;
            }
    }
    
    /*
     On Delete Call
     insert the exceptions recevied 
     parameters are saveresult, DML action insert/update and Class/MethodName,updating record IDs
    */
 /*   
    public static void insertLog(Database.deleteResult [] results,String DMLtype,String errorFrom,set<ID> DBrecIDs)
    {      
      list<Exception_Log__c> excepLogs = new list<Exception_Log__c>();
      set<String> errorMessage = new set<String>();
          
         Exception_Log__c excepLog = new Exception_Log__c();
               excepLog.Created_By__c    = userinfo.getUserId();
               excepLog.Date__c          = system.now();
               excepLog.DML_Action__c    = DMLtype;
               excepLog.Error_Message__c = '';
     
            for (Integer i = 0; i < results.size(); i++) 
            {
                if (results[i].isSuccess()) // if success get IDs
                {
                   if(DBrecIDs.contains(results[i].getId()))
                   {
                       DBrecIDs.remove(results[i].getId()); // remove success IDs to get Error record IDs
                   }
                }
            }
            
           if(DBrecIDs.size() > 0)
           { 
                for (Integer i = 0; i < results.size(); i++) 
                {
                    if (!results[i].isSuccess()) // if failed
                    {     
                        if(!errorMessage.contains(''+results[i].getErrors()) && excepLog.Error_Message__c.length() <= 31500) // if new error message 
                        {                 
                           excepLog.Error_Message__c += errorFrom+'\n'+results[i].getErrors()+'\n \n';  
                           
                        }                                                                             
                    }
                }
                excepLog.Record_IDs__c    = String.valueof(DBrecIDs);
            }
            
            if(excepLog.Error_Message__c != null && excepLog.Error_Message__c != '')
                excepLogs.add(excepLog);
                
            if(excepLogs != null && excepLogs.size() > 0)
            {
                insert excepLogs;
            }
    }
      /*
     On update Call
     insert the exceptions recevied 
     parameters are saveresult, DML action insert/update and Class/MethodName,updating record IDs
    */
  /*  
    public static void insertLog(Database.SaveResult[] results,String DMLtype,String errorFrom,set<String> DBrecIDs)
    {      
      list<Exception_Log__c> excepLogs = new list<Exception_Log__c>();
      set<String> errorMessage = new set<String>();
          
         Exception_Log__c excepLog = new Exception_Log__c();
               excepLog.Created_By__c    = userinfo.getUserId();
               excepLog.Date__c          = system.now();
               excepLog.DML_Action__c    = DMLtype;
               excepLog.Error_Message__c = '';
     
            for (Integer i = 0; i < results.size(); i++) 
            {
                if (results[i].isSuccess()) // if success get IDs
                {
                   if(DBrecIDs.contains(results[i].getId()))
                   {
                       DBrecIDs.remove(results[i].getId()); // remove success IDs to get Error record IDs
                   }
                }
            }
            
           if(DBrecIDs.size() > 0)
           { 
                for (Integer i = 0; i < results.size(); i++) 
                {
                    if (!results[i].isSuccess()) // if failed
                    {     
                        if(!errorMessage.contains(''+results[i].getErrors()) && excepLog.Error_Message__c.length() <= 31500) // if new error message 
                        {                 
                           excepLog.Error_Message__c += errorFrom+'\n'+results[i].getErrors()+'\n \n';  
                           
                        }                                                                             
                    }
                }
                excepLog.Record_IDs__c    = String.valueof(DBrecIDs);
            }
            
            if(excepLog.Error_Message__c != null && excepLog.Error_Message__c != '')
                excepLogs.add(excepLog);
                
            if(excepLogs != null && excepLogs.size() > 0)
            {
                insert excepLogs;
            }
    }
    
	
	// Test method

	public static testmethod void  test1()
	{
	    Exception_Log__c excepLogtrue  = new Exception_Log__c(Created_By__c    = userinfo.getUserId(),Date__c = system.now());
	    Exception_Log__c excepLogfalse = new Exception_Log__c(DML_Action__c    = '123654789895652145120121',Date__c = system.now());
	    map<Id,Exception_Log__c> excepLogmap = new map<ID,Exception_Log__c>();
	    list<Exception_Log__c> excepLoglist = new list<Exception_Log__c>();
	
	
		database.SaveResult [] sr = database.insert(new List<Exception_Log__c>{excepLogtrue,excepLogfalse},false);
		ExceptionLog.insertLog(sr,'insert','debuglog');
	
	 	Exception_Log__c excepLogtrue2  = new Exception_Log__c(Created_By__c    = userinfo.getUserId(),Date__c = system.now());
	    Exception_Log__c excepLogfalse2 = new Exception_Log__c(DML_Action__c    = 'test',Date__c = system.now());
	            
		excepLoglist.add(excepLogtrue2);
		excepLoglist.add(excepLogfalse2);
		
		insert excepLoglist;
		
		for(Exception_Log__c log :excepLoglist)
		{
			if(log.DML_Action__c == 'test')
			log.DML_Action__c = '1252356598745412356987897879789798798789798856556';
			excepLogmap.put(log.ID,log);	
		}
		
		database.SaveResult [] sr2 = database.update(excepLogmap.values(),false);
	    ExceptionLog.insertLog(sr2,'update','debuglog',excepLogmap.keyset());
	    	   
	}    
       
}



Account testacc = new Account(Name = 'tetete',RecordTypeId = '012200000008pQBAAY');

database.SaveResult [] sr = database.insert(new List<Account>{testacc},false);
ExceptionLog.insertLog(sr,'insert','debuglog');

set<ID> DBrecIDs = new set<ID>();
Account testupdate = [SELECT ID,Name from Account WHERE ID= '00120000003dlX9'];

testupdate.Name = '';
DBrecIDs.add(testupdate.ID);

database.SaveResult [] sr = database.update(new List<Account>{testupdate},false);
ExceptionLog.insertLog(sr,'update','debuglog',DBrecIDs);

*/

}