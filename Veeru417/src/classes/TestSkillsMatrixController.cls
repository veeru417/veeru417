@isTest
private class TestSkillsMatrixController {
  static PageReference page;
  static SkillsMatrixController controller;
  static Resource__c barry, tim;
  static User barryUser, timUser;
    
  static {
  	delete [ SELECT Id FROM Resource__c ];
    List<Profile> profileId = [ SELECT Id FROM Profile WHERE Name = 'Standard Platform User' LIMIT 1 ];
  	timUser = new User(FirstName = 'Tim', LastName = 'Barr', Username = 'tim1111@test.com', Email = 'tim1111@test.com', Alias = 'tim',
  		CommunityNickname = 'tbarr', TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1',
  		ProfileId = profileId[0].Id, LanguageLocaleKey = 'en_US'); 
  	barryUser = new User(FirstName = 'Barry', LastName = 'Cade', Username = 'barry2222@test.com', Email = 'barry2222@test.com', Alias = 'barry',
  		CommunityNickname = 'barry', TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1',
  		ProfileId = profileId[0].Id, LanguageLocaleKey = 'en_US'); 
  	insert new User[] { timUser, barryUser };
  	init();
  }
  
  private static void init() {
  	barry = new Resource__c(Name = 'Barry');
  	tim = new Resource__c(Name = 'Tim', User__c = timUser.Id);
    insert new Resource__c[] { barry, tim };
	Skill__c[] skills = new Skill__c[] {
		new Skill__c(Type__c = 'Java', Rating__c = '3',
		Resource__c = tim.Id) };
	insert skills;
    page = new PageReference('SkillsMatrix');
    Test.setCurrentPage(page);
    controller = new SkillsMatrixController();
  }

  static testMethod void testAsUser() {
  	System.runAs(timUser) {
      init();
  	  controller.selectedResourceId = barry.Id;
 	  controller.refresh();
	  System.assert(!controller.isEditable);
  	  controller.selectedResourceId = tim.Id;
  	  controller.refresh();
  	  System.assert(controller.isEditable);
  	}
  }

  static testMethod void testNoResourceForUser() {
  	System.runAs(barryUser) {
      init();
      controller.selectedResourceId = barry.Id;
 	  controller.refresh();
  	  System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
  	}
  }

  static testMethod void testNoSkills() {
  	controller.getResourceOptions();
  	controller.selectedResourceId = barry.Id;
  	controller.refresh();
  	System.assert(controller.selectedSkills.size() > 0);
  	System.assert(controller.isEditable);
  }

  static testMethod void testWithSkills() {
  	controller.getResourceOptions();
  	controller.selectedResourceId = tim.Id;
  	controller.refresh();
  	System.assert(controller.selectedSkills.size() > 0);
  	System.assert(controller.selectedSkills.get(0).Type__c == 'Java');
  }

  static testMethod void testNoResourceSelected() {
  	controller.selectedResourceId = null;
  	PageReference ref = controller.refresh();
  	System.assert(ApexPages.hasMessages());
  }

  static testMethod void testSave() {
  	final String skillRating = '5 - Expert';
  	controller.getResourceOptions();
  	controller.selectedResourceId = barry.Id;
  	controller.refresh();
  	List<Skill__c> selectedSkills = controller.selectedSkills;
  	Skill__c skill = selectedSkills.get(0);
    skill.Rating__c = skillRating;
  	String skillType = skill.Type__c;
  	controller.save();
  	System.assert(ApexPages.hasMessages(ApexPages.Severity.INFO));
  	Skill__c savedSkill = [ SELECT Rating__c FROM Skill__c
  	  WHERE Resource__c = :barry.Id AND
  	  Type__c = :skillType LIMIT 1 ];
  	System.assert(savedSkill != null &&
  	  savedSkill.Rating__c == skillRating);
  }

}