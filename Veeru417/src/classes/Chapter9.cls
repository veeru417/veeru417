public class Chapter9 {
/** Listing 9-1
global class HelloBatchApex implements Database.Batchable<SObject> {
  global Database.QueryLocator start(Database.BatchableContext context) {
    System.debug('start');
    return Database.getQueryLocator(
      [select Name from Proj__c order by Name]);
  }
  global void execute(Database.BatchableContext context,
    List<SObject> scope) {
    System.debug('execute');
    for(SObject rec : scope) {
      Proj__c p = (Proj__c)rec;
      System.debug('Project: ' + p.Name);
    }
  }
  global void finish(Database.BatchableContext context) {
    System.debug('finish');
  }
}
*/

/** Listing 9-2 */
  public static void listing9_2() {
    HelloBatchApex batch = new HelloBatchApex();
    Id jobId = Database.executeBatch(batch);
    System.debug('Started Batch Apex job: ' + jobId);
  }
  
/** Listing 9-3 */
  public static void listing9_3() {
    HelloBatchApex batch = new HelloBatchApex();
    Id jobId = Database.executeBatch(batch, 2);
    System.debug('Started Batch Apex job: ' + jobId);
  }
  
/** Listing 9-4
global class HelloStatefulBatchApex
  implements Database.Batchable<SObject>, Database.Stateful {
  Integer count = 0;
  global Database.QueryLocator start(Database.BatchableContext context) {
    System.debug('start: ' + count);
    return Database.getQueryLocator(
      [select Name from Proj__c order by Name]);
  }
  global void execute(Database.BatchableContext context,
    List<SObject> scope) {
    System.debug('execute: ' + count);
    for(SObject rec : scope) {
      Proj__c p = (Proj__c)rec;
      System.debug('Project ' + count + ': ' + p.Name);
      count++;
    }
  }
  global void finish(Database.BatchableContext context) {
    System.debug('finish: ' + count);
  }
}
*/

/** Listing 9-5
global class ProjectIterable implements Iterator<Proj__c>, Iterable<Proj__c> {
  List<Proj__c> projects { get; set; }
  Integer i;
  public ProjectIterable() { 
    projects = [select Name from Proj__c order by Name ];
    i = 0; 
  }
  global Boolean hasNext() {
    if (i >= projects.size()) {
      return false;
    } else {
      return true; 
    }
  } 
  global Proj__c next() {
    i++;
    return projects[i-1]; 
  }
  global Iterator<Proj__c> Iterator() {
    return this;
  }
}
*/

/** Listing 9-6
global class HelloIterableBatchApex implements Database.Batchable<Proj__c> {
  global Iterable<Proj__c> start(Database.BatchableContext context) {
    System.debug('start');
    return new ProjectIterable();
  }
  global void execute(Database.BatchableContext context,
    List<Proj__c> scope) {
    System.debug('execute');
    for(Proj__c rec : scope) {
      System.debug('Project: ' + rec.Name);
    }
  }
  global void finish(Database.BatchableContext context) {
    System.debug('finish');
  }
}
*/

/** Listing 9-7
public static testmethod void testBatch() {
  Test.startTest();
  HelloBatchApex batch = new HelloBatchApex();
  ID jobId = Database.executeBatch(batch);
  Test.stopTest();
}
*/

/** Listing 9-8
global class HelloSchedulable implements Schedulable {
  global void execute(SchedulableContext sc) {
    HelloBatchApex batch = new HelloBatchApex(); 
    Database.executeBatch(batch);
  }
}
*/

/** Listing 9-9 */
  public static void listing9_9() {
    System.schedule('Scheduled Test', '0 0 1 * * ?', new HelloSchedulable());
  }

/** Listing 9-10
global class MissingTimecardBatch implements Database.Batchable<SObject> {
  global Database.QueryLocator start(Database.BatchableContext context) {
    return Database.getQueryLocator([ select Name, Type__c,
      (select Name, Start_Date__c, End_Date__c
        from Assignments__r where Status__c not in ('Tentative', 'Closed')),
      (select Status__c, Week_Ending__c
        from Timecards__r
      where Status__c in ('Submitted', 'Approved'))
      from Proj__c
    ]);
  }
  global void execute(Database.BatchableContext context,
    List<SObject> scope) {
    List<Missing_Timecard__c> missing = new List<Missing_Timecard__c>();
    for (SObject rec : scope) {
      Proj__c proj = (Proj__c)rec;
      Set<Date> timecards = new Set<Date>();
      if (proj.Assignments__r != null) {
        for (Assignment__c assign : proj.Assignments__r) {
          if (proj.Timecards__r != null) {
            for (Timecard__c timecard : proj.Timecards__r) {
              timecards.add(timecard.Week_Ending__c);
            }
          }
  * Timecards are logged weekly, so the Week_Ending__c field is always
  * a Saturday. We need to convert an assignment, which can contain an
  * arbitrary start and end date, into a start and end period expressed
  * only in terms of Saturdays. To do this, we use the toStartOfWeek
  * method on the Date object, and then add 6 days to reach a Saturday.
  *
          Date startWeekEnding = 
            assign.Start_Date__c.toStartOfWeek().addDays(6);
          Date endWeekEnding =
            assign.End_Date__c.toStartOfWeek().addDays(6);
          Integer weeks = 0;
          while (startWeekEnding.addDays(weeks * 7) < endWeekEnding) {
            Date d = startWeekEnding.addDays(weeks * 7);
            if (d >= Date.today()) {
              break;
            }
            if (!timecards.contains(d)) {
              missing.add(new Missing_Timecard__c(
                Assignment__c = assign.Id,
                Week_Ending__c = d));
            }
            weeks++;
          }
        }
      }
    }
    insert missing;
  }
  global void finish(Database.BatchableContext context) {
  }
}
*/

/** Listing 9-11
  Database.executeBatch(new MissingTimecardBatch());
*/
  
/** Listing 9-12 */
  public static void listing9_12() {
    delete [ select Id from Missing_Timecard__c ];
  }

    testmethod public static void test() {
    	  listing9_2();
    	  listing9_3();
    	  listing9_9();
    	  listing9_12();
    }
}