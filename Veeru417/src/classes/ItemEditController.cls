public with sharing class ItemEditController {
 
  private ApexPages.StandardController controller {get; set;}
  public List<Account> searchResults {get;set;}
  public accountWrapper accw{get;set;}

  public string searchText {get;set;}
    public String myMessage { get; set; }
 
  // standard controller - could also just use custom controller
  public ItemEditController(ApexPages.StandardController controller) {
      
      
  }
  
  public class accountWrapper{
      public Account acc1 {get;set;}
      public string abc;
      
  }
  
  public PageReference searchNew(){
      
      Account Acc=Database.query('select id,name,phone from account where name LIKE \'%'+searchText+'%\'limit 1');
      system.debug('@@@Acc'+Acc);
      accw = new accountWrapper();
      accw.acc1 =Acc;
      accw.abc='stringto display';
      system.debug('@@@accwaccw'+accw);
      return null;
  }
 
  // fired when the search button is clicked
  public PageReference search() {
  if(searchText=='')
  {
  
  myMessage = 'WARNING:Please Enter a search string';
  }
  
  else
  {
  String qry = 'select id, name,BillingState,phone,Website from Account where name LIKE \'%'+searchText+'%\' Order By Name';
        
    
    searchResults = Database.query(qry);
  
  }
    return null;
  }
 
  // fired when the save records button is clicked
  
 
  // takes user back to main record 
   public PageReference save() {
   System.Debug('hai method');
    return null;
  }
  public PageReference cancel() {
    return null;
  }
  
 
 
 
}