public with sharing class CohortTest2
{
    public boolean showMe{get;set;}
    public CohortTest2()
    {
        showMe = false;
    }
    
    public void test()
    {
        showMe = !showMe;
        //return new PageReference('/apex/Cohort_SetupWizard2');
        //return null;
    }
}