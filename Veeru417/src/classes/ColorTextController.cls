public class ColorTextController {
    public string caseID{get;set;}
    public string colorText{get;set;}
    
    public ColorTextController(ApexPages.StandardController controller){
        if (ApexPages.currentPage().getParameters().ContainsKey('Id')) 
            caseID = ApexPages.currentPage().getParameters().get('Id');
        Account acc=[select id,name from account where id=:caseID];
        
        colorText=acc.name;
    }

}