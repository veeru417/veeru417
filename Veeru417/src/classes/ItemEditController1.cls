public with sharing class ItemEditController1 {
 
  private ApexPages.StandardController controller {get; set;}
  public List<Account> searchResults {get;set;}

  public string searchText {get;set;}
    public String myMessage { get; set; }
 
  // standard controller - could also just use custom controller
  public ItemEditController1(ApexPages.StandardController controller) { }
 
  // fired when the search button is clicked
  public PageReference search() {
  if(searchText=='')
  {
  
  myMessage = 'WARNING:Please Enter a search string';
  }
  
  else
  {
  
    String qry = 'select id, name,BillingState,phone,Website from Account ' +
      'where name LIKE \'%'+searchText+'%\' order by name';
    searchResults = Database.query(qry);
  
  }
    return null;
  }
 
   
 
 
 
}