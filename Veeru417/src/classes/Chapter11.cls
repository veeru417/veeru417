public class Chapter11 {

/** Listing 11-1
ConnectorConfig config = new ConnectorConfig();
config.setUsername(user);
config.setPassword(pass);
EnterpriseConnection connection = Connector.newConnection(config);
*/

/** Listing 11-2
SforceService binding = new SforceService();
LoginResult result = binding.login(user, pass + securityToken);
binding.SessionHeaderValue = new SessionHeader();
binding.SessionHeaderValue.sessionId = result.sessionId;
binding.Url = result.serverUrl;
*/

/** Listing 11-3	
List<Proj__c> projects = new ArrayList<Proj__c>();
QueryResult qr = connection.query("SELECT Id, Name FROM Proj__c");
boolean done = false;
if (qr.getSize() > 0) {
  while (!done) {
    SObject[] records = qr.getRecords();
    if (records != null) {
      for (SObject record : records) {
        projects.add((Proj__c)record);
      }
      if (qr.isDone()) {
        done = true;
      } else {
        qr = connection.queryMore(qr.getQueryLocator());
      }
    }
  }
}
*/

/** Listing 11-4
List<Proj__c> projects = new List<Proj__c>();
QueryResult qr = binding.query("SELECT Id, Name FROM Proj__c");
Boolean done = false;
if (qr.size > 0) {
  while (!done) {
    sObject[] records = qr.records;
    if (records != null) {
    foreach (sObject record in records) {
      projects.Add((Proj__c)record);
    }
    if (qr.done) {
      done = true;
    } else {
      qr = binding.queryMore(qr.queryLocator);
    }
  }
}
*/

/** Listing 11-5
connection.setQueryOptions(2000);
*/

/** Listing 11-6
binding.QueryOptionsValue = new QueryOptions();
binding.QueryOptionsValue.batchSize = 2000;
binding.QueryOptionsValue.batchSizeSpecified = true;
*/

/** Listing 11-7
String newResourceId = null;
Contact contact = new Contact();
contact.setFirstName(firstName);
contact.setLastName(lastName);
SaveResult[] result = connection.create(
  new SObject[] { contact });
if (result != null && result.length == 1) {
  if (result[0].isSuccess()) {
    Resource__c resource = new Resource__c();
    resource.setActive__c(true);
    resource.setName(firstName + " " + lastName);
    resource.setContact__c(result[0].getId());
    SaveResult[] result2 = connection.create(
      new SObject[] { resource });
    if (result2 != null && result2.length == 1) {
      if (result2[0].isSuccess()) {
        newResourceId = result2[0].getId();
      } else {
        System.out.println("Failed to create resource: " +
          result2[0].getErrors()[0].getMessage());
      }
    }
  } else {
    System.out.println("Failed to create contact: " +
      result[0].getErrors()[0].getMessage());
  }
}
*/

/** Listing 11-8
String newResourceId = null;
Contact contact = new Contact();
contact.FirstName = firstName;
contact.LastName = lastName;
SaveResult[] result = binding.create(
  new sObject[] { contact });
if (result != null && result.Length == 1) {
  if (result[0].success) {
    Resource__c resource = new Resource__c();
    resource.Active__c = true;
    resource.Name = firstName + " " + lastName;
    resource.Contact__c = result[0].id;
    SaveResult[] result2 = binding.create(
      new sObject[] { resource });
    if (result2 != null && result2.Length == 1) {
      if (result2[0].success) {
        newResourceId = result2[0].id;
      } else {
        Console.WriteLine("Failed to create resource: " + 
          result2[0].errors[0].message);
      }
    }
  } else {
    Console.WriteLine("Failed to create contact: " + 
      result[0].errors[0].message);
  }
}
*/

/** Listing 11-9
global class Custom {
  webservice static ID createProject(String name) {
    Proj__c proj = new Proj__c(Name = name);
    insert proj;
    return proj.Id;
  }
}
*/

/** Listing 11-10
ConnectorConfig config = new ConnectorConfig();
config.setUsername(user);
config.setPassword(pass);
Connector.newConnection(config);
config.setServiceEndpoint(com.sforce.soap.Custom.Connector.END_POINT);
SoapConnection sconn = new SoapConnection(config);
String projectId = sconn.createProject("Test Project");
*/

/** Listing 11-11
public String CreateProject(String sessionId, String name) {
  CustomService service = new CustomService();
  service.SessionHeaderValue = new CustomWS.SessionHeader();
  service.SessionHeaderValue.sessionId = sessionId;
  return service.createProject(name);
}
*/

/** Listing 11-12
public void createObject(String name, String pluralName) {
  try {
    ConnectorConfig config = new ConnectorConfig();
    config.setUsername(user);
    config.setPassword(pass);    
    com.sforce.soap.enterprise.Connector.newConnection(config);
    config.setServiceEndpoint(Connector.END_POINT);
    MetadataConnection connection = new MetadataConnection(config);
    CustomObject obj = new CustomObject();
    obj.setFullName(name + "__c");
    obj.setLabel(name);
    obj.setPluralLabel(pluralName);
    obj.setDeploymentStatus(DeploymentStatus.Deployed);
    obj.setSharingModel(SharingModel.ReadWrite);
    CustomField nameField = new CustomField();
    nameField.setType(FieldType.AutoNumber);
    nameField.setLabel("Name");
    obj.setNameField(nameField);
    AsyncResult[] result = connection.create(
      new Metadata[] { obj });
    if (result == null) {
      System.out.println("create failed");
      return;
    }
    boolean done = false;
    AsyncResult[] status = null;
    long waitTime = 1000;
    while (!done) {
      status = connection.checkStatus(
        new String[] { result[0].getId() });
      if (status != null) {
        done = status[0].isDone();
        if (status[0].getStatusCode() != null) {
          System.out.println("Error: " +
            status[0].getStatusCode() + ": " +
            status[0].getMessage());
        }
        Thread.sleep(waitTime);
        waitTime *= 2;
        System.out.println("Current state: " +
          status[0].getState());
      }
    }
    System.out.println("Created object: " +
      status[0].getId());
  } catch (Throwable t) {
    t.printStackTrace();
  }
}
*/

/** Listing 11-13
curl https://na6.salesforce.com/services/data/v20.0\
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1" 
{
  "sobjects" : "/services/data/v20.0/sobjects",
  "search" : "/services/data/v20.0/search",
  "query" : "/services/data/v20.0/query",
  "recent" : "/services/data/v20.0/recent"
}
*/

/** Listing 11-14
curl https://na6.salesforce.com/services/data/v20.0/sobjects/Proj__c 
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1"
*/

/** Listing 11-15
curl https://na6.salesforce.com/services/data/v20.0\
  /sobjects/Proj__c/a008000000CTwEw?fields=Name,Status__c\
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1"
{
  "attributes" : {
    "type" : "Proj__c",
    "url" : "/services/data/v20.0/sobjects/Proj__c/a008000000CTwEwAAL"
  },
  "Name" : "GenePoint",
  "Status__c" : "In Progress",
  "Id" : "a008000000CTwEwAAL"
}
*/

/** Listing 11-16
curl https://na6.salesforce.com/services/data/v20.0\
  /sobjects/Resource__c/Resource_ID__c/100000\
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1" 
*/

/** Listing 11-17
curl https://na6.salesforce.com/services/data/v20.0\
  /query?q=SELECT+Name+FROM+Proj__c\
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1"
*/

/** Listing 11-18
echo '{ "Name": "Test Project" }' |\
  curl -X POST -H 'Content-type: application/json'\
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1" -d @-\
  https://na6.salesforce.com/services/data/v20.0/sobjects/Proj__c
{
  "id" : "a008000000Fy8oyAAB",
  "errors" : [ ],
  "success" : true
}
*/

/** Listing 11-19
echo '{ "Name": "Updated Test Project" }' |\
  curl -X PATCH -H 'Content-type: application/json'\
  -H 'Authorization: OAuth '$TOKEN -H "X-PrettyPrint:1" -d @-\
  https://na6.salesforce.com/services/data/v20.0\
  /sobjects/Proj__c/a008000000Fy8oyAAB
*/

/** Listing 11-20
echo '{ "Name": "Izzy Impatient" }' |\
  curl -X PATCH -H 'Content-type: application/json'\
  -H "Authorization: OAuth "$TOKEN -H "X-PrettyPrint:1" -d @-\
  https://na6.salesforce.com/services/data/v20.0\
  /sobjects/Resource__c/Resource_ID__c/100050
{
  "id" : "a018000000OjBKLAA3",
  "errors" : [ ],
  "success" : true
}
*/

/** Listing 11-21
curl -X DELETE\
  -H 'Authorization: OAuth '$TOKEN -H "X-PrettyPrint:1"\
  https://na6.salesforce.com/services/data/v20.0\
  /sobjects/Proj__c/a008000000Fy8oyAAB
*/

/** Listing 11-22
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.UpsertResult;
import com.sforce.soap.enterprise.sobject.Resource__c;
import com.sforce.soap.enterprise.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
public class IntegrationDemo {
  EnterpriseConnection connection;
  public void login(String user, String pass, String securityToken) {
    ConnectorConfig config = new ConnectorConfig();
    config.setUsername(user);
    config.setPassword(pass + securityToken);
    try {
      connection = Connector.newConnection(config);
    } catch (ConnectionException e) {
      e.printStackTrace();
    }
  }
  public void processImportFile(String jsonFile) {
    List<SObject> changes = new ArrayList<SObject>();
    try {
      String json = readFileAsString(jsonFile);
      JSONArray array = new JSONArray(json);
      for (int i=0; i<array.length(); i++) {
        changes.add(importResource(array.getJSONObject(i)));
      }
      if (changes.size() > 0) {
        UpsertResult[] results = connection.upsert("Resource_ID__c",
          changes.toArray(new SObject[changes.size()]));
        int line = 0;
        for (UpsertResult result : results) {
          System.out.print(line + ": ");
          if (!result.isSuccess()) {
            for (com.sforce.soap.enterprise.Error e
              : result.getErrors()) {
              System.out.println(e.getStatusCode() + ": " +
                e.getMessage());
            }
          } else {
            System.out.println("success");
          }
          line++;
        }
      }
    } catch (Throwable t) {
      t.printStackTrace();
    }
  }
  private Resource__c importResource(JSONObject rec)
    throws JSONException {
    Resource__c result = new Resource__c();
    result.setResource_ID__c(Double.valueOf(
      rec.getInt("ResourceID")));
    result.setActive__c(rec.getBoolean("Active"));
    return result;
  }
  private static String readFileAsString(String filePath)
    throws IOException {
    StringBuffer fileData = new StringBuffer(1000);
    BufferedReader reader = new BufferedReader(
      new FileReader(filePath));
    char[] buf = new char[2048];
    int numRead = 0;
    while((numRead = reader.read(buf)) != -1) {
      fileData.append(buf, 0, numRead);
    }
    reader.close();
    return fileData.toString();
  }
  public static void main(String[] args) {
    IntegrationDemo demo = new IntegrationDemo();
    demo.login("USERNAME", "PASSWORD", "SECURITYTOKEN");
    demo.processImportFile("import.json");
  }
}
*/

/** Listing 11-23
[
  {
    "ResourceID": 100000,
    "Active": false
  },
  {
    "ResourceID": 100001,
    "Active": false
  }
]
*/

  testmethod public static void test() {}
       
}