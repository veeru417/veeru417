public class pagenav{

public ApexPages.standardController controller {get; set;}

public string SSI_1{get;set;}
public string  SSI_2{get;set;}
public string  SSI_3{get;set;}
public string  SSI_4{get;set;}
public string  SSI_5{get;set;}
public string  SSI_6{get;set;}
public string  SSI_7{get;set;}
public string  SSI_8{get;set;}
public string  SSI_9{get;set;}
public string  SSI_10{get;set;}
public string  SSI_11{get;set;}
public string  SSI_12{get;set;}
public string  SSI_13{get;set;}
public string  SSI_14{get;set;}
public string  SSI_15{get;set;}
public string  SSI_16{get;set;}
public string  SSI_17{get;set;}
public string  SSI_18{get;set;}

public pagenav(ApexPages.StandardController stdController) {

}



public List<SelectOption> getconditions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('0 Strongly Disagree','0 Strongly Disagree'));
        options.add(new SelectOption('1 Disagree','1 Disagree'));
        options.add(new SelectOption('2 Neutral','2 Neutral'));
        options.add(new SelectOption('3 Agree','3 Agree'));
        options.add(new SelectOption('4 Strongly Agree','4 Strongly Agree'));
        return options;
     }

public PageReference step1() {
        return Page.jStep1;
    }
public PageReference step2() {
        return Page.jStep2;
    }
    
public PageReference step3() {
        return Page.jStep3;
    }    


}