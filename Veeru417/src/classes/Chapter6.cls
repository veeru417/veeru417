public class Chapter6 {

  static {
  	Resource__c tim = new Resource__c(Name = 'Tim Barr', Hourly_Cost_Rate__c = 100);
  	insert tim;
  }
  
/** Listing 6-1 */
  public static void listing6_1() {
    Integer i = [ SELECT COUNT() FROM Timecard__c ];
    System.debug(i);
  }

/** Listing 6-2 */
  public static void listing6_2() {
    AggregateResult r = [ SELECT SUM(Total_Hours__c) Total
      FROM Timecard__c ];
    System.debug(r.get('Total'));
  }
  
/** Listing 6-3 */
  public static void listing6_3() {
    for (AggregateResult r : [ SELECT Region__c FROM Resource__c
      GROUP BY Region__c ]) {
      System.debug(r.get('Region__c'));
    }
  }

/** Listing 6-4 */
  public static void listing6_4() {
    for (AggregateResult r : [ SELECT Resource__r.Region__c, 
      SUM(Total_Hours__c) FROM Timecard__c
      GROUP BY Resource__r.Region__c ]) {
      System.debug(r.get('Region__c') + ' ' + r.get('expr0'));
    }
  }

/** Listing 6-5 */
  public static void listing6_5() {
    for (AggregateResult r : [ SELECT Highest_Education_Level__c ed,
      AVG(Hourly_Cost_Rate__c) FROM Resource__c
      GROUP BY Highest_Education_Level__c 
      HAVING AVG(Hourly_Cost_Rate__c) > 100 ]) {
      System.debug(r.get('ed') + ' ' + r.get('expr0'));
    }
  }

/** Listing 6-6 */
  public static void listing6_6() {
    for (AggregateResult r : [ SELECT Project__r.Status__c, Resource__r.Region__c, 
      SUM(Total_Hours__c) hours, COUNT(Id) recs,
      GROUPING(Project__r.Status__c) status, GROUPING(Resource__r.Region__c) region
      FROM Timecard__c
      GROUP BY ROLLUP( Project__r.Status__c, Resource__r.Region__c )
      ORDER BY GROUPING(Project__r.Status__c), GROUPING(Resource__r.Region__c) ]) {
      System.debug(LoggingLevel.INFO, 
        r.get('Status__c') + ' ' + r.get('Region__c') + ' ' +
        r.get('region') + ' ' + r.get('status') + ' ' +
        r.get('hours') + ' ' + r.get('recs'));
    }
  }

/** Listing 6-7
16:04:43.207|USER_DEBUG|[7]|INFO|Green West 0 0 230.0 6
16:04:43.207|USER_DEBUG|[7]|INFO|Green Central 0 0 152.0 4
16:04:43.207|USER_DEBUG|[7]|INFO|Yellow Central 0 0 109.0 3
16:04:43.207|USER_DEBUG|[7]|INFO|Green null 1 0 382.0 10
16:04:43.207|USER_DEBUG|[7]|INFO|Yellow null 1 0 109.0 3
16:04:43.207|USER_DEBUG|[7]|INFO|null null 1 1 491.0 13
*/

/** Listing 6-8
16:06:56.003|USER_DEBUG|[7]|INFO|Green Central 0 0 152.0 4
16:06:56.003|USER_DEBUG|[7]|INFO|Green West 0 0 230.0 6
16:06:56.004|USER_DEBUG|[7]|INFO|Yellow Central 0 0 109.0 3
16:06:56.004|USER_DEBUG|[7]|INFO|Green null 1 0 382.0 10
16:06:56.004|USER_DEBUG|[7]|INFO|Yellow null 1 0 109.0 3
16:06:56.004|USER_DEBUG|[7]|INFO|null West 0 1 230.0 6
16:06:56.004|USER_DEBUG|[7]|INFO|null Central 0 1 261.0 7
16:06:56.005|USER_DEBUG|[7]|INFO|null null 1 1 491.0 13
*/

/** Listing 6-9 */
  public static void listing6_9() {
    List<Proj__c> result = [ SELECT Name, Account__r.Name
      FROM Proj__c ];
  }

/** Listing 6-10 */
  public static void listing6_10() {
    List<Proj__c> result = [ SELECT Name, Account__r.Name
      FROM Proj__c
      WHERE Account__c != null ];
  }

/** Listing 6-11 */
  public static void listing6_11() {
    List<Account> result = [ SELECT Id, Name,
      (SELECT Id FROM Projects__r WHERE Status__c = 'Yellow')
      FROM Account ];
  }

/** Listing 6-12 */
  public static void listing6_12() {
    List<Account> result = [ SELECT Id, Name
      FROM Account
      WHERE Id IN
        (SELECT Account__c FROM Proj__c WHERE Status__c = 'Yellow') ];
  }

/** Listing 6-13 */
  public static void listing6_13() {
    List<Account> result = [ SELECT Id, Name
      FROM Account
      WHERE Id NOT IN
        (SELECT Account__c FROM Proj__c WHERE Status__c = 'Green') ];
  }

/** Listing 6-14 */
  public static void listing6_14() {
    List<Timecard__c> result = [ SELECT Project__r.Name, Week_Ending__c, Total_Hours__c
      FROM Timecard__c
      WHERE Resource__c IN
        (SELECT Resource__c FROM Assignment__c WHERE Role__c = 'Consultant') ];
  }

/** Listing 6-15 */
  public static void listing6_15() {
    List<Timecard__c> result = [ SELECT Project__r.Name, Week_Ending__c, Total_Hours__c
      FROM Timecard__c
      WHERE Resource__c IN
        (SELECT Id FROM Resource__c WHERE Hourly_Cost_Rate__c > 100) ];
  }

/** Listing 6-16 */
  public static void listing6_16() {
    List<Proj__c> result = [ SELECT Id, Name
      FROM Proj__c
      WHERE Requested_Skills__c INCLUDES ('Apex;Java;C#', 'Python') ];
  }

/** Listing 6-17 */
public static void listing6_17() {
  List<List<SObject>> result = [
                                  FIND 'Chicago'
                                  RETURNING Proj__c(Name), Resource__c(Name)
                                  ];
  List<Proj__c> projects = (List<Proj__c>)result[0];
  for (Proj__c project : projects) {
    System.debug('Project: ' + project.Name);
  }
  List<Resource__c> resources = (List<Resource__c>)result[1];
  for (Resource__c resource : resources) {
    System.debug('Resource: ' + resource.Name);
  }
}

/** Listing 6-18 */
public static void listing6_18() {
  Resource__c tim = [ SELECT Id
                        FROM Resource__c
                        WHERE Name = 'Tim Barr' LIMIT 1 ];
  Skill__c skill1 = new Skill__c(Resource__c = tim.Id, Type__c = 'Java', Rating__c = '3 - Average');
  Skill__c skill2 = new Skill__c(Resource__c = tim.Id, Rating__c = '4 - Above Average');
  Skill__c[] skills = new  Skill__c[] { skill1, skill2 };
  Database.SaveResult[] saveResults = Database.insert(skills, false);
  for (Integer i=0; i<saveResults.size(); i++) {
    Database.SaveResult saveResult = saveResults[i];
    if (!saveResult.isSuccess()) {
      Database.Error err = saveResult.getErrors()[0];
      System.debug('Skill ' + i + ' insert failed: ' + err.getMessage());
    } else {
      System.debug('Skill ' + i + ' insert succeeded: new Id = ' + saveResult.getId());
    }
  }
}

/** Listing 6-19 */
static void printRecordCount() {
  System.debug([ SELECT count() FROM Resource__c ] + ' records');
}

public static void listing6_19() {
  printRecordCount();
  Savepoint sp = Database.setSavepoint(); 

  delete [ SELECT Id FROM Resource__c ];
  printRecordCount();

  Database.rollback(sp); 
  printRecordCount();
}

/** Listing 6-20 */
public static void listing6_20() {
  Resource__c tim = [ SELECT Id, Hourly_Cost_Rate__c
                        FROM Resource__c
                        WHERE Name = 'Tim Barr' LIMIT 1
                        FOR UPDATE ];
  tim.Hourly_Cost_Rate__c += 20;
  update tim;
}

/** Listing 6-21
    SELECT ParentId, UserOrGroupId, AccessLevel
      FROM Proj__Share
      WHERE Parent.Name = 'GenePoint'
*/

/** Listing 6-22
    SELECT Id, Type, RelatedId
      FROM Group
*/

/** Listing 6-23
User tim = [ SELECT Id FROM User
             WHERE Name = 'Tim Barr' LIMIT 1 ];
Proj__c genePoint = [ SELECT Id FROM Proj__c
                      WHERE Name = 'GenePoint' LIMIT 1 ];
Proj__Share share = new Proj__Share(
    ParentId = genePoint.Id,
    UserOrGroupId = tim.Id,
    rowCause = Proj__Share.rowCause.My_Sharing_Reason__c,
    AccessLevel = 'Read');
insert share;
*/

/** Listing 6-24 */
public static void listing6_24() {
  User you = [ SELECT Email
                 FROM User
                 WHERE Id = :UserInfo.getUserId()
                 LIMIT 1 ];
  Messaging.SingleEmailMessage mail =
    new Messaging.SingleEmailMessage(); 
  mail.setToAddresses(new String[] { you.Email });
  mail.setSubject('Test message');
  mail.setPlainTextBody('This is a test');
  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
}

/** Listing 6-25
public static void listing6_25() {
  User you = [ SELECT Email
             FROM User
             WHERE Id = :UserInfo.getUserId()
             LIMIT 1 ];
  EmailTemplate template = [ SELECT Id
                           FROM EmailTemplate
                           WHERE DeveloperName = 'Test_Template'
                             LIMIT 1 ];
  Messaging.SingleEmailMessage mail =
    new Messaging.SingleEmailMessage(); 
  mail.templateId = template.Id;
  mail.targetObjectId = you.Id;
  mail.setSaveAsActivity(false);
  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
}
*/

/** Listing 6-26
public static void listing6_26() {
  User you = [ SELECT Email
             FROM User
             WHERE Id = :UserInfo.getUserId()
             LIMIT 1 ];
  EmailTemplate template = [ SELECT Id
                           FROM EmailTemplate
                           WHERE DeveloperName = 'Test_Template'
                             LIMIT 1 ];
  Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
  mail.templateId = template.Id;
  mail.targetObjectIds = new Id[] { you.Id };
  mail.setSaveAsActivity(false);
  Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail }); 
}
*/

/** Listing 6-27
global class MyEmailService implements
  Messaging.InboundEmailHandler {
  global Messaging.InboundEmailResult
    handleInboundEmail(Messaging.InboundEmail email,
      Messaging.InboundEnvelope envelope) {
    Messaging.InboundEmailResult result = new
      Messaging.InboundEmailresult();
    Messaging.SingleEmailMessage outbound = new
      Messaging.SingleEmailMessage();
    outbound.toAddresses = new String[] { email.replyTo };
    outbound.setSubject('Re: ' + email.subject);
    outbound.setHtmlBody('<p>This reply was generated by Apex.'
      + 'You wrote:</p><i>' + email.plainTextBody + '</i>');
    Messaging.sendEmail(new Messaging.SingleEmailMessage[]
      { outbound });
    return result;
  }
}
*/

/** Listing 6-28 */
public static void listing6_28() {
  List<Resource__c> resources = Database.query('SELECT Id, Name FROM Resource__c');
  for (Resource__c resource : resources) {
    System.debug(resource.Id + ' ' + resource.Name);
  }
  List<SObject> projects = Database.query('SELECT Id, Name FROM Proj__c');
  for (SObject project : projects) {
    System.debug(project.get('Id') + ' ' + project.get('Name'));
  }
}

/** Listing 6-29 */
public static void listing6_29() {
  List<List<SObject>> result = Search.query(
        'FIND \'Chicago\' '
        + 'RETURNING Resource__c(Name), Proj__c(Name)');
  for (List<SObject> records : result) {
    for (SObject record : records) {
      System.debug(record.get('Name'));
    }
  }
}

/** Listing 6-30 */
public static void listing6_30() {
  Map<String, Schema.SObjectType> objects = Schema.getGlobalDescribe();
  Schema.DescribeSObjectResult objInfo = null;
  for (Schema.SObjectType obj : objects.values()) {
    objInfo = obj.getDescribe();
    System.debug(objInfo.getName() + ' [' + objInfo.getLabel() + ']');
  }
}

/** Listing 6-31 */
public static void listing6_31() {
  Map<String, Schema.SObjectField> fields = Schema.SObjectType.Proj__c.fields.getMap();
  Schema.DescribeFieldResult fieldInfo = null;
  for (Schema.SObjectField field : fields.values()) {
    fieldInfo = field.getDescribe();
    System.debug(fieldInfo.getName() 
          + ' [' + fieldInfo.getLabel() + '] ' 
          + fieldInfo.getType().name()
          + '(' + fieldInfo.getPrecision()
          + ', ' + fieldInfo.getScale() + ')');
  }
}

/** Listing 6-32 */
public static void listing6_32() {
  Schema.DescribeSObjectResult res = Resource__c.SObjectType.getDescribe(); 
  List<Schema.ChildRelationship> relationships = res.getChildRelationships(); 
  for (Schema.ChildRelationship relationship : relationships) {
    System.debug(relationship.getField() + ', ' + relationship.getChildSObject());
  }
}

/** Listing 6-33 */
public static void listing6_33() {
  Schema.DescribeFieldResult fieldInfo = Schema.SObjectType.Skill__c.fields.Type__c;
  List<Schema.PicklistEntry> picklistValues = fieldInfo.getPicklistValues();
  for (Schema.PicklistEntry picklistValue : picklistValues) {
    System.debug(picklistValue.getLabel());
  }
}

/** Listing 6-34 */
public static void listing6_34() {
  Schema.DescribeSObjectResult sobj = Resource__c.SObjectType.getDescribe();
  List<Schema.RecordTypeInfo> recordTypes = sobj.getRecordTypeInfos();
  for (Schema.RecordTypeInfo recordType : recordTypes) {
    System.debug(recordType.getName());
  }
}

/** Listing 6-35 */
public static void listing6_35() {
  try {
    insert new ConfigSetting__c(Name = 'Default', Debug__c = false);
  } catch (Exception e) {}
}

/** Listing 6-36 */
public static void listing6_36() {
  listing6_35();
  ConfigSetting__c cfg = ConfigSetting__c.getValues('Default');
  System.debug(cfg.Debug__c);
}

/** Listing 6-37 */
public static void listing6_37() {
  listing6_35();
  ConfigSetting__c cfg = ConfigSetting__c.getValues('Default');
  cfg.Debug__c = false;
  update cfg;
}

/** Listing 6-38 */
public static void listing6_38() {
  listing6_35();
  ConfigSetting__c cfg = ConfigSetting__c.getValues('Default');
  delete cfg;
}

/** Listing 6-39 */
public static void listing6_39() {
  try {
    insert new HierarchySetting__c(
      SetupOwnerId = UserInfo.getUserId(),
      Debug__c = true);
  } catch (Exception e) {}
}

/** Listing 6-40
trigger handleTimecardNotifications
  on Timecard__c (after update) {
  for (Timecard__c timecard : trigger.new) {
    if (timecard.Status__c !=
      trigger.oldMap.get(timecard.Id).Status__c &&
      (timecard.Status__c == 'Approved' ||
      timecard.Status__c == 'Rejected')) {
      Resource__c resource =
        [ SELECT Contact__r.Email FROM Resource__c
          WHERE Id = :timecard.Resource__c LIMIT 1 ];
      Proj__c project =
        [ SELECT Name FROM Proj__c
          WHERE Id = :timecard.Project__c LIMIT 1 ];
      User user = [ SELECT Name FROM User
          WHERE Id = :timecard.LastModifiedById LIMIT 1 ];
      Messaging.SingleEmailMessage mail = new
        Messaging.SingleEmailMessage();
      mail.toAddresses = new String[]
        { resource.Contact__r.Email };
      mail.setSubject('Timecard for '
        + timecard.Week_Ending__c + ' on '
        + project.Name);
      mail.setHtmlBody('Your timecard was changed to '
        + timecard.Status__c + ' status by '
        + user.Name);
      Messaging.sendEmail(new Messaging.SingleEmailMessage[]
        { mail });
    }
  }
}
*/

    testmethod public static void test() {
      listing6_1();
      listing6_2();
      listing6_3();
      listing6_4();
      listing6_5();
      listing6_6();
      listing6_9();
      listing6_10();
      listing6_11();
      listing6_12();
      listing6_13();
      listing6_14();
      listing6_15();
      listing6_16();
      listing6_17();
      listing6_18();
      listing6_19();
      listing6_20();
      listing6_24();
      listing6_28();
      listing6_29();
      listing6_30();
      listing6_31();
      listing6_32();
      listing6_33();
      listing6_34();
      listing6_35();
      listing6_36();
      listing6_37();
      listing6_38();
      listing6_39();
    }
}