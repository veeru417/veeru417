global class HelloBatchApex implements Database.Batchable<SObject> {
    
    
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        System.debug('start');
        return Database.getQueryLocator([select Name from Myrcrtmnt__vf__c order by Name]);
    }

    global void execute(Database.BatchableContext context, List<SObject> scope) {
        System.debug('execute');
        for(SObject rec : scope) {
            Myrcrtmnt__vf__c p = (Myrcrtmnt__vf__c)rec;
            System.debug('Project: ' + p.Name);
            p.Name = p.name;
        }
    }
    
    global void finish(Database.BatchableContext context) {
        system.debug('@@@soqlQuery Limit'+limits.getQueries());
    }

  public static testmethod void testBatch() {
    Test.startTest();
    //HelloBatchApex batch = new HelloBatchApex();
    //ID jobId = Database.executeBatch(batch);
    Test.stopTest();
      
  }
}