trigger taskOnObject on Task (after update)
{
List<ID> objectToBeUpdatedIDList = new List<ID>();
 
for(Task t: Trigger.new)
{
if(t.status == 'Completed')
{
objectToBeUpdatedIDList.add(t.whatid);
}
}
Map<ID, programmeorg__c> objMap = new Map<ID, programmeorg__c>([select id, TaskStatus__c from programmeorg__c where id in :objectToBeUpdatedIDList and Sponsoring_group__c=true ]);
List<programmeorg__c> objList = new List<programmeorg__c>();
for(Task t: Trigger.new)
{
if(t.status == 'Completed')
{
programmeorg__c o = objMap.get(t.whatid);
o.TaskStatus__c = 'task completed';

o.status__c='red';
//update remaining fields.
objList.add(o);
}
}
if(objList.size() > 0)
update objList;
}