trigger AccountTrigger1 on Account (before insert, before update) {
    
    List<Account> lst = trigger.new;
    for(Account acc:lst){
        
        if(acc.BillingCity != null && acc.ShippingCity == null){
            acc.ShippingCity = acc.BillingCity;
        }
        
    }
}