trigger duplicatecheck on Account (before insert) {
Account[] acct=Trigger.new;
Set<String> PhoneSet = new Set<String>();
for(Account a:acct)
{ 
PhoneSet.add(a.Phone);
}

List<Account> duplicateAccountList = [Select a.Name, a.Phone From Account a
where a.Phone IN :PhoneSet];
Set<string> duplicatePhone = new Set<string>();

       for(Account a : duplicateAccountList)
       {
        duplicatePhone.add(a.Phone);
       }

       for(Account a : acct)
       {
            if(duplicatePhone.contains(a.Phone))
            {
                a.Phone.addError('Record already exist with same phone no');
            }
       }
}