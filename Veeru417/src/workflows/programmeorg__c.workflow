<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>mm</fullName>
        <description>mm</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sponsoring_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Auto_response</template>
    </alerts>
    <fieldUpdates>
        <fullName>Sponsoring_Group_identified</fullName>
        <field>Sponsoring_group__c</field>
        <literalValue>1</literalValue>
        <name>Sponsoring Group identified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>status_update</fullName>
        <field>Status__c</field>
        <literalValue>Green</literalValue>
        <name>status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>status_update1</fullName>
        <field>Status__c</field>
        <literalValue>Red</literalValue>
        <name>status update1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Sponsoring Group Profile</fullName>
        <actions>
            <name>Sponsor_the_Programme_task</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>programmeorg__c.userrole__c</field>
            <operation>equals</operation>
            <value>CEO</value>
        </criteriaItems>
        <criteriaItems>
            <field>programmeorg__c.userrole__c</field>
            <operation>equals</operation>
            <value>Operations Director</value>
        </criteriaItems>
        <criteriaItems>
            <field>programmeorg__c.userrole__c</field>
            <operation>equals</operation>
            <value>Programme Director</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Deadline</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Appoint_Programme_Board</fullName>
        <assignedTo>directorabc@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>programmeorg__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Appoint Programme Board</subject>
    </tasks>
    <tasks>
        <fullName>Appoint_SRO</fullName>
        <assignedTo>directorabc@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>programmeorg__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Appoint SRO</subject>
    </tasks>
    <tasks>
        <fullName>Deadline</fullName>
        <assignedTo>directorabc@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>programmeorg__c.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Deadline</subject>
    </tasks>
    <tasks>
        <fullName>Sponsor_the_Programme_task</fullName>
        <assignedTo>directorabc@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Fill the details like sponsoring group will document their perspective on the Programme and particular interests, document the level of support they will be able to give to the programme and confirm</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sponsor the Programme</subject>
    </tasks>
</Workflow>
