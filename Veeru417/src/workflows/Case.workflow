<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X1_hour_warning_before_milestone_expires</fullName>
        <description>1 hour warning before milestone expires</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/support_Happy_customer</template>
    </alerts>
    <alerts>
        <fullName>new_case_is_created</fullName>
        <description>new case is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>veeru417@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Auto_response</template>
    </alerts>
    <rules>
        <fullName>Auto-response</fullName>
        <actions>
            <name>new_case_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
