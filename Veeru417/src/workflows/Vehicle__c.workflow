<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification</fullName>
        <ccEmails>sravs.14@gmail.com</ccEmails>
        <description>Email notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_step</fullName>
        <field>Approval_comments__c</field>
        <formula>&quot;Record Approved&quot;</formula>
        <name>Approved step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_step</fullName>
        <field>Approval_comments__c</field>
        <formula>&quot;Record Recall&quot;</formula>
        <name>Recall step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_step</fullName>
        <field>Approval_comments__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>Reject step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_field</fullName>
        <field>Approval_comments__c</field>
        <formula>&quot;Approval Sent&quot;</formula>
        <name>Update Approval field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
