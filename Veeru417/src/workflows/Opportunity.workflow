<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Opportunity_Alert</fullName>
        <ccEmails>rare417@gmail.com</ccEmails>
        <description>New Opportunity Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>veeru417@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/My_Std_Email_Temp</template>
    </alerts>
    <alerts>
        <fullName>recall_me</fullName>
        <ccEmails>sasya.ravi@gmail.com</ccEmails>
        <description>recall me</description>
        <protected>false</protected>
        <recipients>
            <recipient>directorabc@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/My_Std_Email_Temp</template>
    </alerts>
    <fieldUpdates>
        <fullName>Opt_Name</fullName>
        <field>Name</field>
        <formula>IF(

NOT(ISPICKVAL(Product_Interest__c,&apos;&apos;)),

Name +&apos; - &apos;+

TEXT(Product_Interest__c) +&apos; - &apos;+Text(DATEVALUE(CreatedDate)),

Name+&apos; - &apos;+TEXT(Business_Interest__c) +&apos; - &apos;+Text (DATEVALUE(CreatedDate))

)</formula>
        <name>Opt Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_probability</fullName>
        <field>Probability</field>
        <formula>0.1</formula>
        <name>update probability</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set naming convention for  Opportunity</fullName>
        <actions>
            <name>New_Opportunity_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opt_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOT(ISBLANK(  Name  )), ISBLANK(  Opportunity_ID__c  )

)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>probability update</fullName>
        <actions>
            <name>update_probability</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>nthng</fullName>
        <assignedTo>operationdir@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>User.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>nthng</subject>
    </tasks>
</Workflow>
