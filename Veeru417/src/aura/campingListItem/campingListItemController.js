({
	packItem  : function(component, event, helper) {
		var itemVal = component.get("v.item",true);
        itemVal.Packed__c = true;
        component.set("v.item",itemVal);
        var btnClicked = event.getSource();
        btnClicked.set("v.disabled",true);
	}
})